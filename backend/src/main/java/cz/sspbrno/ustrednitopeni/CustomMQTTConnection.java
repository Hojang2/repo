package cz.sspbrno.ustrednitopeni;

import cz.sspbrno.ustrednitopeni.sensors.HumiditySensor;
import cz.sspbrno.ustrednitopeni.sensors.LightSensor;
import cz.sspbrno.ustrednitopeni.sensors.OxygenSensor;
import cz.sspbrno.ustrednitopeni.sensors.TemperatureSensor;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CustomMQTTConnection {

    public static final String ipAddress = "tcp://10.10.1.1:1883";

    public CustomMQTTConnection() throws Exception {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        DBConnector dbConnector = new DBConnector();
        TemperatureSensor tempSensor = new TemperatureSensor(options);
        LightSensor lightSensor = new LightSensor(options);
        HumiditySensor humSensor = new HumiditySensor(options);
        OxygenSensor oxySensor = new OxygenSensor(options);

        new Thread(() -> {
            while (true) {
                try {
                    tempSensor.run();
                    if (tempSensor.getMessage().equals(""))
                        continue;
                    HashMap<Type, String> hashMap = new HashMap<>();
                    hashMap.put(Type.TEMPERATURE ,tempSensor.getMessage());
                    dbConnector.saveData(hashMap);
                    tempSensor.clearMessage();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    lightSensor.run();
                    if (lightSensor.getMessage().equals(""))
                        continue;
                    HashMap<Type, String> hashMap = new HashMap<>();
                    hashMap.put(Type.LIGHT ,lightSensor.getMessage());
                    dbConnector.saveData(hashMap);
                    lightSensor.clearMessage();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    humSensor.run();
                    if (humSensor.getMessage().equals(""))
                        continue;

                    HashMap<Type, String> hashMap = new HashMap<>();
                    hashMap.put(Type.HUMIDITY, humSensor.getMessage());
                    dbConnector.saveData(hashMap);
                    humSensor.clearMessage();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                try {
                    oxySensor.run();
                    HashMap<Type, String> hashMap = new HashMap<>();
                    if (oxySensor.getMessage().equals(""))
                        continue;
                    hashMap.put(Type.OXYGEN ,oxySensor.getMessage());
                    dbConnector.saveData(hashMap);
                    oxySensor.clearMessage();
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
